<?php

    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');
    $animal = new Animal("shaun");
    echo "Name : " . $animal-> name . "<br>";
    echo "legs : " . $animal-> legs . "<br>";
    echo "cold blooded : " . $animal-> cold_blooded . "<br> <br>";

    $Frog = new frog("buduk");
    echo "Name : " . $Frog-> name . "<br>";
    echo "legs : " . $Frog-> legs . "<br>";
    echo "cold blooded : " . $Frog-> cold_blooded . "<br>";
    echo $Frog->Jump();
    
    $Ape = new ape("kera sakti");
    echo "Name : " . $Ape-> name . "<br>";
    echo "legs : " . $Ape-> legs . "<br>";
    echo "cold blooded : " . $Ape-> cold_blooded . "<br>";
    echo $Ape->yell();

?>